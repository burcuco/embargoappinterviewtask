# README #

### What is this repository for? ###

* Embargo App Interview Task
* 1.0

### Technologies ###

* Java
* MVP
* Retrofit
* Dagger2
* Rx Java
* Realm Database
* Android Lifecycle
* Android Support Libraries
* Glide

### Contact Info ###
* me@burcu.co 
* burcuuturkmen@gmail.com

Thank you!