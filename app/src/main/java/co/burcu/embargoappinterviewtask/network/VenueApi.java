package co.burcu.embargoappinterviewtask.network;

import co.burcu.embargoappinterviewtask.model.VenueResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Burcu Yalcinkaya on 21:17 9/1/18.
 */
public interface VenueApi {

    @GET("venues.json")
    Observable<VenueResponse> getVenues();

}
