package co.burcu.embargoappinterviewtask.presenter;

/**
 * Created by Burcu Yalcinkaya on 00:32 9/2/18.
 */
public interface MainPresenterInterface {
    void getVenues();
}
