package co.burcu.embargoappinterviewtask.presenter;

import android.annotation.SuppressLint;

import co.burcu.embargoappinterviewtask.model.VenueResponse;
import co.burcu.embargoappinterviewtask.network.VenueApi;
import co.burcu.embargoappinterviewtask.network.VenueClient;
import co.burcu.embargoappinterviewtask.view.MainViewInterface;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Burcu Yalcinkaya on 00:31 9/2/18.
 */
public class MainPresenter implements MainPresenterInterface {

    MainViewInterface mvi;

    public MainPresenter(MainViewInterface mvi) {
        this.mvi = mvi;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getVenues() {
        getObservable().subscribeWith(getObserver());
    }

    private Observable<VenueResponse> getObservable() {
        return VenueClient.getRetrofit().create(VenueApi.class)
                .getVenues()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableObserver<VenueResponse> getObserver() {
        return new DisposableObserver<VenueResponse>() {

            @Override
            public void onNext(@NonNull VenueResponse venueList) {
                mvi.displayVenues(venueList);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mvi.displayError("Error fetching Venue Data");
            }

            @Override
            public void onComplete() {
                mvi.hideProgressBar();
            }
        };
    }
}
