package co.burcu.embargoappinterviewtask.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Burcu Yalcinkaya on 21:41 9/1/18.
 */
public class Venue  {

    @SerializedName("City")
    private String city;

    @SerializedName("Name")
    private String name;

    @SerializedName("Group")
    private String group;

    @SerializedName("ZIP")
    private String zip;

    @SerializedName("Country")
    private String country;

    @SerializedName("Longitude")
    private Double longitude;

    @SerializedName("Latitude")
    private Double latitude;

    @SerializedName("Scheme")
    private String scheme;

    @SerializedName("Street")
    private String street;

    @SerializedName("VenueID")
    private int venueID;

    @SerializedName("Images")
    private List<String> images;

    @SerializedName("Categories")
    private List<String> categories;


    public Venue(String city, String name, String group, String zip, String country, Double longitude, Double latitude, String scheme, String street, int venueID, List<String> images, List<String> categories) {
        super();
        this.city = city;
        this.name = name;
        this.group = group;
        this.zip = zip;
        this.country = country;
        this.longitude = longitude;
        this.latitude = latitude;
        this.scheme = scheme;
        this.street = street;
        this.images = images;
        this.categories = categories;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getVenueID() {
        return venueID;
    }

    public void setVenueID(int venueID) {
        this.venueID = venueID;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }
}
