package co.burcu.embargoappinterviewtask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Burcu Yalcinkaya on 23:21 9/1/18.
 */
public class VenueResponse {

    @SerializedName("Venues")
    @Expose
    private List<Venue> venues = null;

    public VenueResponse() {

    }

    public VenueResponse(List<Venue> venues) {
        super();
        this.venues = venues;
    }

    public List<Venue> getVenues() {
        return venues;
    }

    public void setVenues(List<Venue> venues) {
        this.venues = venues;
    }
}
