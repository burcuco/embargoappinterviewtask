package co.burcu.embargoappinterviewtask.realm;

/**
 * Created by Burcu Yalcinkaya on 21:02 9/2/18.
 */

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import java.util.List;

import co.burcu.embargoappinterviewtask.realm.model.RealmVenue;
import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    public void clearAll() {

        realm.beginTransaction();
        realm.delete(RealmVenue.class);
        realm.commitTransaction();
    }

    public RealmResults<RealmVenue> getVenues() {

        return realm.where(RealmVenue.class).findAll();
    }

    public void insertVenue(List<RealmVenue> venues) {
        for (RealmVenue venue : venues) {
            realm.beginTransaction();
            realm.copyToRealm(venue);
            realm.commitTransaction();
        }
    }

}
