package co.burcu.embargoappinterviewtask.view;

import co.burcu.embargoappinterviewtask.model.VenueResponse;

public interface MainViewInterface {

    void showToast(String s);

    void showProgressBar();

    void hideProgressBar();

    void displayVenues(VenueResponse venueList);

    void displayError(String errorMessage);
}