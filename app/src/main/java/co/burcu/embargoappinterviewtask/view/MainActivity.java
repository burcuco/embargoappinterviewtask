package co.burcu.embargoappinterviewtask.view;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.burcu.embargoappinterviewtask.R;
import co.burcu.embargoappinterviewtask.adapter.VenueAdapter;
import co.burcu.embargoappinterviewtask.model.Venue;
import co.burcu.embargoappinterviewtask.model.VenueResponse;
import co.burcu.embargoappinterviewtask.presenter.MainPresenter;
import co.burcu.embargoappinterviewtask.realm.RealmController;
import co.burcu.embargoappinterviewtask.realm.model.RealmVenue;
import co.burcu.embargoappinterviewtask.util.LocationHelper;
import co.burcu.embargoappinterviewtask.util.TimerHelper;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static co.burcu.embargoappinterviewtask.util.TimerHelper.ONE_HOUR;

public class MainActivity extends AppCompatActivity implements MainViewInterface {

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    @BindView(R.id.venuesRecyclerView)
    RecyclerView venuesRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private RecyclerView.Adapter adapter;

    private MainPresenter mainPresenter;


    private LocationHelper locationHelper;
    private LocationManager locationManager;
    private LocationListener locationListener;

    private TimerHelper timerHelper;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_browse:
                    return true;
                case R.id.navigation_explore:
                    showToast(getString(R.string.message_menu_explore));
                    return true;
                case R.id.navigation_map:
                    showToast(getString(R.string.message_menu_map));
                    return true;
                case R.id.navigation_profile:
                    showToast(getString(R.string.message_menu_profile));
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(MainActivity.this);

        setupLocation();
        setupMVP();
        setupViews();
        getVenueList();
    }

    private void setupLocation() {
        locationHelper = new LocationHelper(MainActivity.this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    locationHelper.setLocation((float) location.getLatitude(), (float) location.getLongitude());
                    if (adapter != null)
                        adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, 1);
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }

    }

    private void setupMVP() {
        mainPresenter = new MainPresenter(this);
    }

    private void setupViews() {
        venuesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    //Date is smaller than 1 hour ->Realm
    //Otherwise -> Retrofit
    private void getVenueList() {
        if (isOnlineDataAvailable()) {
            mainPresenter.getVenues();
        } else {
            adapter = new VenueAdapter(RealmController.with(this).getVenues(), MainActivity.this);
            venuesRecyclerView.setAdapter(adapter);
            hideProgressBar();
        }
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayVenues(VenueResponse venueList) {
        if (venueList != null) {
            timerHelper.setTimeStamp();
            prepareRealmData(venueList.getVenues());
            adapter = new VenueAdapter(MainActivity.this, venueList.getVenues());
            venuesRecyclerView.setAdapter(adapter);
        } else {
            showToast(getString(R.string.default_error_message));
        }
    }

    private void prepareRealmData(List<Venue> venues) {
        List<RealmVenue> realmVenueList = new ArrayList<>();
        int i = 0;
        for (Venue venue : venues) {
            RealmVenue realmVenue = new RealmVenue();
            realmVenue.setId(i++ + System.currentTimeMillis());
            realmVenue.setCity(venue.getCity());
            realmVenue.setImage(venue.getImages().get(0));
            realmVenue.setLatitude(venue.getLatitude());
            realmVenue.setLongitude(venue.getLongitude());
            realmVenue.setName(venue.getName());
            realmVenueList.add(realmVenue);
        }
        RealmController.with(this).clearAll();
        RealmController.with(this).insertVenue(realmVenueList);
    }

    @Override
    public void displayError(String errorMessage) {
        showToast(getString(R.string.default_error_message));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

            }

        } else {
            locationHelper.setLocation(0, 0);
            if (adapter != null)
                adapter.notifyDataSetChanged();
        }
    }

    public boolean isOnlineDataAvailable() {
        timerHelper = new TimerHelper(this);
        long previousTimeStamp = timerHelper.getTimeStamp();
        if (previousTimeStamp == 0) return true;
        long currentTime = System.currentTimeMillis();
        return Math.abs(currentTime - previousTimeStamp) >= ONE_HOUR;
    }
}
