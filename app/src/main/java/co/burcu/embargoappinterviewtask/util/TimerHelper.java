package co.burcu.embargoappinterviewtask.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Burcu Yalcinkaya on 22:12 9/2/18.
 */
public class TimerHelper {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    public static int ONE_HOUR = 3000 * 60 * 60;

    public TimerHelper(Context context) {
        sharedPreferences = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();
    }

    public long getTimeStamp() {
        long currentTime = sharedPreferences.getLong("currenttime", 0);
        return currentTime;
    }

    public void setTimeStamp() {
        long currentTimeMillis = System.currentTimeMillis();
        editor.putLong("currenttime", currentTimeMillis);
        editor.commit();
    }
}
