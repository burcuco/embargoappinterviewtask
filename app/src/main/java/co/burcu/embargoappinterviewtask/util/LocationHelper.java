package co.burcu.embargoappinterviewtask.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Burcu Yalcinkaya on 17:09 9/2/18.
 */
public class LocationHelper {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public LocationHelper(Context context) {
        sharedPreferences = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();
    }

    public Location getLocation() {
        float latitude = sharedPreferences.getFloat("latitude", 0);
        float longitude = sharedPreferences.getFloat("longitude", 0);
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }

    public void setLocation(float latitude, float longitude) {
        editor.putFloat("latitude", latitude);
        editor.putFloat("longitude", longitude);
        editor.commit();
    }

    public float calculateDistance(Location firstLocation, Location secondLocation) {
        float distanceInMeters = firstLocation.distanceTo(secondLocation);
        distanceInMeters = distanceInMeters / 1000;
        return distanceInMeters;
    }
}
