package co.burcu.embargoappinterviewtask.adapter;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import co.burcu.embargoappinterviewtask.R;
import co.burcu.embargoappinterviewtask.model.Venue;
import co.burcu.embargoappinterviewtask.realm.model.RealmVenue;
import co.burcu.embargoappinterviewtask.util.LocationHelper;

/**
 * Created by Burcu Yalcinkaya on 22:11 9/1/18.
 */
public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.VenueHolder> {

    private List<Venue> venueList;
    private List<RealmVenue> realmVenueList;
    private Context context;
    private boolean isOnline = true;

    public VenueAdapter(Context context, List<Venue> venueList) {
        this.venueList = venueList;
        this.context = context;
        this.isOnline = true;
    }

    public VenueAdapter(List<RealmVenue> realmVenueList, Context context) {
        this.realmVenueList = realmVenueList;
        this.context = context;
        this.isOnline = false;
    }

    @Override
    public VenueHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_venue, parent, false);
        VenueHolder venueHolder = new VenueHolder(v);
        return venueHolder;
    }

    @Override
    public void onBindViewHolder(VenueHolder holder, int position) {
        if (isOnline) {
            holder.titleTextView.setText(venueList.get(position).getName());
            float distance = getDistance(venueList.get(position).getLatitude(), venueList.get(position).getLongitude());
            if (distance == 0) {
                holder.distanceTextView.setText(venueList.get(position).getCity());
            } else {
                holder.distanceTextView.setText(String.format("%s / %s KM", venueList.get(position).getCity(), distance));
            }
            Glide.with(context).load(venueList.get(position).getImages().get(0)).into(holder.venueImageView);

        } else {
            holder.titleTextView.setText(realmVenueList.get(position).getName());
            float distance = getDistance(realmVenueList.get(position).getLatitude(), realmVenueList.get(position).getLongitude());
            if (distance == 0) {
                holder.distanceTextView.setText(realmVenueList.get(position).getCity());
            } else {
                holder.distanceTextView.setText(String.format("%s / %s KM", realmVenueList.get(position).getCity(), distance));
            }
            Glide.with(context).load(realmVenueList.get(position).getImage()).into(holder.venueImageView);

        }
    }

    @Override
    public int getItemCount() {
        if (isOnline) {
            return venueList.size();
        } else {
            return realmVenueList.size();
        }

    }

    private float getDistance(double latitude, double longitude) {
        LocationHelper locationHelper = new LocationHelper(context);
        Location firstLocation = locationHelper.getLocation();
        if (firstLocation == null || firstLocation.getLatitude() == 0.0) return 0;
        Location secondLocation = new Location("");
        secondLocation.setLongitude(longitude);
        secondLocation.setLatitude(latitude);
        return locationHelper.calculateDistance(firstLocation, secondLocation);
    }

    public class VenueHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView titleTextView, distanceTextView;
        ImageView venueImageView;

        public VenueHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            titleTextView = v.findViewById(R.id.tvTitle);
            distanceTextView = v.findViewById(R.id.tvDistance);
            venueImageView = v.findViewById(R.id.ivVenue);
            venueImageView.setClipToOutline(true);
        }

        @Override
        public void onClick(View view) {
            if (isOnline) {
                Toast.makeText(view.getContext(), venueList.get(getLayoutPosition()).getName(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(view.getContext(), realmVenueList.get(getLayoutPosition()).getName(), Toast.LENGTH_SHORT).show();
            }


        }
    }
}
